def dec_vers_binhex(nb_dec):
    # A compléter !

def bin_vers_dechex(nb_bin):
    # A compléter !

def hex_vers_decbin(nb_hex):
    # A compléter !

def affiche(d, b, h):
    print("------------------------------------------")
    print("   Décimal   |   Binaire   | Hexadécimal")
    print(f" {d:^11} | {b:^11} | {h:^11}")
    print("------------------------------------------")
 
 d = 47
 b, h = dec_vers_binhex(d)
 affiche(d, b, h)
 b = '0b11001011'
 d, h = bin_vers_dechex(b)
 affiche(d, b, h)
 h = '0x2F'
 d, b = hex_vers_decbin(h)
 affiche(d, b, h)