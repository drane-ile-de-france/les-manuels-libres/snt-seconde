// Récupération du formulaire
var formulaire = document.getElementById("monFormulaire");

// Événement de soumission du formulaire
formulaire.addEventListener("submit", function(event) {
  event.preventDefault(); // Empêche l'envoi du formulaire

  // Récupération des valeurs saisies
  var nom = document.getElementById("nom").value;
  var email = document.getElementById("email").value;

  // Validation des données
  if (nom === "" || email === "") {
    alert("Veuillez remplir tous les champs du formulaire.");
  } else {
    // Traitement des données
    alert("Nom : " + nom + "\nEmail : " + email);
    formulaire.reset(); // Réinitialisation du formulaire
  }
});

